package net.potatohed.Poisons;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class poisonsConsumeEvent implements Listener
{
	Plugin plugin;
	private enum Effects
	{
		Wither, Poison, Slow, Harm
	}

	public poisonsConsumeEvent(Plugin p)
	{
		plugin = p;
	}
	@EventHandler
	public void onPLayerConsumeEvent(PlayerItemConsumeEvent e)
	{
		if(e.getItem().getType() != Material.POTION)
			return;
		if(!e.getItem().getItemMeta().hasLore())
			return;
		for (String line : e.getItem().getItemMeta().getLore())
		{
			if (line.contains("&P&S"))
			{
				Effects effect = null;
				switch (line)
				{
				case "&P&S&W":
					effect = Effects.Wither;
					break;				
				case "&P&S&P":
					effect = Effects.Poison;
					break;				
				case "&P&S&S":
					effect = Effects.Slow;		
					break;
				case "&P&S&H":
					effect = Effects.Harm;					
					break;				
				}
				final Effects feffect = effect;
				final Player p = e.getPlayer();
				for (OfflinePlayer op : Bukkit.getServer().getOperators())
					if (op.isOnline())
						op.getPlayer().sendMessage(p.getName() + " Has been Poisoned");	
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin ,new Runnable()
				{
					@SuppressWarnings("deprecation")
					public void run()
					{
						PotionEffect potion;
						switch (feffect)
						{		
						case Wither:
							potion = new PotionEffect(PotionEffectType.WITHER, 20 * 20, 1);
							potion.apply(p);
							break;
						case Poison:
							potion = new PotionEffect(PotionEffectType.POISON, 20 * 20, 1);
							potion.apply(p);
							break;
						case Slow:
							potion = new PotionEffect(PotionEffectType.SLOW, 20 * 20, 1);
							potion.apply(p);
							break;
						case Harm:
							potion = new PotionEffect(PotionEffectType.HARM, 5, 50);
							potion.apply(p);
											
							Bukkit.getServer().broadcastMessage("&c" + p.getName() + "&c Has Died of Poison");
							OfflinePlayer op = p.getPlayer();
							op.setBanned(true);
							p.kickPlayer("You have been poisoned, and died. Please make an appeal on the website");
							break;
						default:
							break;							
						}

					}
				}, (20 * 60 * 5));
			}
		}
	}
}