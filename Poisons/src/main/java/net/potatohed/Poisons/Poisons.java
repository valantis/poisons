package net.potatohed.Poisons;

import java.io.IOException;
import java.util.LinkedList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.MetricsLite;

import net.potatohed.CustomLore.*;

public class Poisons extends JavaPlugin
{
	Plugin plugin = this;
	public void onEnable()
	{
		registerRecipies();
		registerCustomLore();
		registerEvents();

		
		try {
		    MetricsLite metrics = new MetricsLite(this);
		    metrics.start();
		} catch (IOException e) {
		    // Failed to submit the stats :-(
		}

	}

	public void onDisable()
	{
		HandlerList.unregisterAll(this);

	}



	@SuppressWarnings("deprecation")
	//Will fix this when bukkit gets it out
	private void registerRecipies()
	{

		byte neg1 = -1;

		ShapelessRecipe wither = new ShapelessRecipe(getPotion());
		ShapelessRecipe poison = new ShapelessRecipe(getPotion());
		ShapelessRecipe slow = new ShapelessRecipe(getPotion());
		ShapelessRecipe harm = new ShapelessRecipe(getPotion());


		wither = wither.addIngredient(new MaterialData(Material.PAPER, (byte) 50 )).addIngredient(new MaterialData(Material.POTION , neg1 ));

		poison = poison.addIngredient(new MaterialData(Material.PAPER, (byte) 51 )).addIngredient(new MaterialData(Material.POTION , neg1 ));

		slow = slow.addIngredient(new MaterialData(Material.PAPER, (byte) 52 )).addIngredient(new MaterialData(Material.POTION , neg1 ));

		harm = harm.addIngredient(new MaterialData(Material.PAPER, (byte) 53 )).addIngredient(new MaterialData(Material.POTION , neg1 ));



		ShapedRecipe witherToken = new ShapedRecipe(getWitherToken());
		ShapedRecipe poisonToken = new ShapedRecipe(getPoisonToken());
		ShapedRecipe slowToken = new ShapedRecipe(getSlowToken());
		ShapedRecipe harmToken = new ShapedRecipe(getHarmToken());

		witherToken.shape("ABC","DED","FGF");




		//Layer 1
		witherToken = witherToken.setIngredient('A', Material.BROWN_MUSHROOM).setIngredient('B', new MaterialData(Material.COAL , neg1   )).setIngredient('C', Material.RED_MUSHROOM);
		//Layer 2
		witherToken = witherToken.setIngredient('D', new MaterialData(Material.INK_SACK , (byte) 8 )).setIngredient('D', Material.PAPER);
		//Layer 3
		witherToken = witherToken.setIngredient('F' ,new MaterialData(Material.SKULL_ITEM)).setIngredient('G', new MaterialData(Material.SKULL_ITEM , (byte) 1));


		poisonToken.shape("ABC","DED","FGF");

		//Layer 1
		poisonToken = poisonToken.setIngredient('A', Material.BROWN_MUSHROOM).setIngredient('B', new MaterialData(Material.COAL , neg1)).setIngredient('C', Material.RED_MUSHROOM);
		//Layer 2
		poisonToken = poisonToken.setIngredient('D', new MaterialData(Material.INK_SACK , (byte) 8 )).setIngredient('E', Material.PAPER);
		//Layer 3
		poisonToken = poisonToken.setIngredient('F' , Material.POTION, 8228).setIngredient('G', new MaterialData(Material.SKULL_ITEM));

		slowToken.shape("ABC","DED","FGF");

		//Layer 1
		slowToken = slowToken.setIngredient('A', Material.BROWN_MUSHROOM).setIngredient('B', new MaterialData(Material.COAL , neg1 )).setIngredient('C', Material.RED_MUSHROOM);
		//Layer 2
		slowToken = slowToken.setIngredient('D', new MaterialData(Material.INK_SACK , (byte) 8 )).setIngredient('E', Material.PAPER);
		//Layer 3
		slowToken = slowToken.setIngredient('F' , Material.POTION, 8266).setIngredient('G', new MaterialData(Material.SKULL_ITEM));		


		harmToken.shape("ABC","DED","FGF");

		//Layer 1
		harmToken = harmToken.setIngredient('A', Material.BROWN_MUSHROOM).setIngredient('B', new MaterialData(Material.COAL , neg1)).setIngredient('C', Material.RED_MUSHROOM);
		//Layer 2
		harmToken = harmToken.setIngredient('D', new MaterialData(Material.INK_SACK , (byte) 8 )).setIngredient('E', Material.PAPER);
		//Layer 3
		harmToken = harmToken.setIngredient('F' , Material.POTION, 8236).setIngredient('G', new MaterialData(Material.SKULL_ITEM , (byte) 1));

		getServer().addRecipe(wither);
		getServer().addRecipe(poison);
		getServer().addRecipe(slow);
		getServer().addRecipe(harm);
		getServer().addRecipe(witherToken);
		getServer().addRecipe(poisonToken);
		getServer().addRecipe(slowToken);
		getServer().addRecipe(harmToken);



	}
	private void registerCustomLore() 
	{
		if (getServer().getPluginManager().getPlugin("CustomLore") != null)
		{
			getServer().getScheduler().scheduleSyncDelayedTask(this,new Runnable()
			{
				public void run()
				{
					CustomLore customLore  = (CustomLore) getServer().getPluginManager().getPlugin("CustomLore");
					customLore.registerItem(getWitherToken());
					customLore.registerItem(getPoisonToken());
					customLore.registerItem(getSlowToken());
					customLore.registerItem(getHarmToken());

					customLore.registerString("&P&S&W");
					customLore.registerString("&P&S&P");
					customLore.registerString("&P&S&S");
					customLore.registerString("&P&S&H");

				}
			}, 30L);
		}

	}
	private void registerEvents() {

		poisonsConsumeEvent consume = new poisonsConsumeEvent(this.plugin);
		poisonsCraftEvent craft = new poisonsCraftEvent();	
		poisonsBrewEvent brew = new poisonsBrewEvent();
		poisonsPickUpEvent pickup = new poisonsPickUpEvent();


		getServer().getPluginManager().registerEvents(consume , this );
		getServer().getPluginManager().registerEvents(craft , this );
		getServer().getPluginManager().registerEvents(brew , this );
		getServer().getPluginManager().registerEvents(pickup , this );
	}
	// Item Stack Getters
	public static ItemStack getWitherToken()
	{
		ItemStack token = new ItemStack(Material.PAPER , 1, (byte) 50 );
		ItemMeta imeta = Bukkit.getItemFactory().getItemMeta(token.getType());
		imeta.setDisplayName("&cPouch of necrotic dust");
		LinkedList<String> ilore = new LinkedList<String>();
		ilore.add("&cAdd this to any potion to cause the");
		ilore.add("&cimbider to become withered after a ");
		ilore.add("&cshort period of time.");
		ilore.add("");
		ilore.add("&cAct quickly as the dust only last ");
		ilore.add("&ca few hours.");
		imeta.setLore(ilore);
		token.setItemMeta(imeta);

		return token;


	}
	public static ItemStack getPoisonToken()
	{
		ItemStack token = new ItemStack(Material.PAPER , 1, (byte) 51 );
		ItemMeta imeta = Bukkit.getItemFactory().getItemMeta(token.getType());
		imeta.setDisplayName("&cPouch of poisonous dust");
		LinkedList<String> ilore = new LinkedList<String>();
		ilore.add("&cAdd this to any potion to cause the");
		ilore.add("&cimbider to become poisioned after a ");
		ilore.add("&cshort period of time.");
		ilore.add("");
		ilore.add("&cAct quickly as the dust only last ");
		ilore.add("&ca few hours.");
		imeta.setLore(ilore);



		token.setItemMeta(imeta);

		return token;


	}
	public static ItemStack getSlowToken()
	{
		ItemStack token = new ItemStack(Material.PAPER , 1, (byte) 52 );
		ItemMeta imeta = Bukkit.getItemFactory().getItemMeta(token.getType());
		imeta.setDisplayName("&cPouch of sleeping dust");
		LinkedList<String> ilore = new LinkedList<String>();
		ilore.add("&cAdd this to any potion to cause the");
		ilore.add("&cimbider to become slowed after a ");
		ilore.add("&cshort period of time.");
		ilore.add("");
		ilore.add("&cAct quickly as the dust only last ");
		ilore.add("&ca few hours.");
		imeta.setLore(ilore);



		token.setItemMeta(imeta);

		return token;


	}
	public static ItemStack getHarmToken()
	{
		ItemStack token = new ItemStack(Material.PAPER , 1, (byte) 53 );
		ItemMeta imeta = Bukkit.getItemFactory().getItemMeta(token.getType());
		imeta.setDisplayName("&cPouch of deathly dust");
		LinkedList<String> ilore = new LinkedList<String>();
		ilore.add("&cAdd this to any potion to cause the");
		ilore.add("&cimbider to die after a ");
		ilore.add("&cshort period of time.");
		ilore.add("");
		ilore.add("&cAct quickly as the dust only last ");
		ilore.add("&ca few hours.");
		imeta.setLore(ilore);



		token.setItemMeta(imeta);

		return token;


	}

	public static ItemStack getPotion()
	{
		ItemStack potion = new ItemStack(Material.POTION);
		potion.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 1);
		return potion;
	}








}
