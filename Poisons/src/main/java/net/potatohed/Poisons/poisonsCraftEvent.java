package net.potatohed.Poisons;

import java.util.LinkedList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class poisonsCraftEvent implements Listener 
{
	@EventHandler
	public void onCraftEvent( CraftItemEvent e)
	{
		ItemStack result = e.getRecipe().getResult();

		if (result.getType() != Material.POTION)
			return;
		if(!result.containsEnchantment(Enchantment.DAMAGE_ALL))
			return;

		CraftingInventory ci = e.getInventory();
		ItemStack[] items = ci.getContents();

		ItemStack potion = null;	
		for (ItemStack item : items)
		{
			if (item.getType() == Material.POTION)
				potion = item;
		}

		//String Pname = Potion.fromItemStack(potion).getType().getEffectType().getName();
		ItemMeta imeta = potion.getItemMeta();
		List<String> iLore;
		if (imeta.hasLore())
			iLore = imeta.getLore();
		else
			iLore = new LinkedList<String>();

		ItemStack paper = ci.getItem(ci.first(Material.PAPER));
		
		Player p = (Player) e.getViewers().get(0);

		if (paper.isSimilar(Poisons.getWitherToken()) && p.hasPermission("Poisons.Type.Wither"))
			iLore.add("&P&S&W");
		else 
		{
			e.setCancelled(true);
			p.sendMessage("&c You do not have permission to make this");
		}
		if (paper.isSimilar(Poisons.getPoisonToken()) && p.hasPermission("Poisons.Type.Poison"))
			iLore.add("&P&S&P");
		else
		{
			e.setCancelled(true);
			p.sendMessage("&c You do not have permission to make this");
		}
		if (paper.isSimilar(Poisons.getSlowToken()) && p.hasPermission("Poisons.Type.Slow"))
			iLore.add( "&P&S&S");
		else
		{
			e.setCancelled(true);
			p.sendMessage("&c You do not have permission to make this");
		}
		if (paper.isSimilar(Poisons.getHarmToken()) && p.hasPermission("Poisons.Type.Harm"))
			iLore.add("&P&S&H");
		else 
		{
			e.setCancelled(true);
			p.sendMessage("&c You do not have permission to make this");
		}

		//Set Lore Back
		imeta.setLore(iLore);
		//Set Name Back
		potion.setItemMeta(imeta);

		//Set Item Back
		e.setCurrentItem(potion);



	}
}

